
=begin
Begin-Doc
Name: MST::CDI::Admin
Type: module
Description: Core data administrative RPC routines
End-Doc
=cut

package MST::CDI::Admin;
require Exporter;
use strict;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

use Local::UsageLogger;
use Local::AuthSrv;
use Local::Encode;
use Local::SimpleRPC;

use MST::Env;

@ISA    = qw(Exporter);
@EXPORT = qw();

BEGIN {
    &LogAPIUsage();
}

# Begin-Doc
# Name: new
# Type: function
# Description: Creates object
# Syntax: $cdi = new MST::CDI::Admin()
# End-Doc
sub new {
    my $self  = shift;
    my $class = ref($self) || $self;
    my $tmp   = {};

    &LogAPIUsage();

    return bless $tmp, $class;
}

# Begin-Doc
# Name: SimpleAdminRPC
# Type: function
# Description: Helper routine to set up RPC connection for user apps
# Syntax: $rpc = &SimpleAdminRPC()
# Access: internal
# End-Doc
sub SimpleAdminRPC {
    my $self = shift;

    if ( $self->{admin_rpc} ) {
        return $self->{admin_rpc};
    }

    my $rpchost;
    my $env = &MST_Env();

    if ( $env eq "dev" ) {
        $rpchost = "itweb-dev.mst.edu";
    }
    elsif ( $env eq "test" ) {
        $rpchost = "itweb-test.mst.edu";
    }
    else {
        $rpchost = "itweb.mst.edu";
    }

    $self->{admin_rpc} = new Local::SimpleRPC::Client(
        base_url => "https://${rpchost}/auth-cgi-bin/cgiwrap/coreloader/rpc",
        retries  => 2
    );

    return $self->{admin_rpc};
}

# Begin-Doc
# Name: TriggerLoadSync
# Type: method
# Description: Attempts to trigger a load sync for a list of tables
# Syntax: $res = $obj->TriggerLoadSync(@tables);
# Returns: failure message on failure, 0 or undef otherwise
# End-Doc
sub TriggerLoadSync {
    my $self   = shift;
    my @tables = @_;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    my @req = ();
    foreach my $table (@tables) {
        push( @req, table => $table );
    }
    my $info = $rpc->TriggerLoadSync(@req);
    return $info;
}

1;
