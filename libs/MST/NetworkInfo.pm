# Begin-Doc
# Name: MST::NetworkInfo
# Type: module
# Description: object to manage access to all network config info
# Comments: This has access to subnet configs, ip allocations, ethernet address lookup, etc.
# End-Doc

package MST::NetworkInfo;
require Exporter;
use strict;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

use Local::UsageLogger;
use Local::Env;
use Local::AuthSrv;
use Local::Encode;
use Local::SimpleRPC;

@ISA    = qw(Exporter);
@EXPORT = qw();

BEGIN {
    &LogAPIUsage();
}

# Begin-Doc
# Name: new
# Type: function
# Description: Creates object
# Syntax: $netinfo = new MST::NetworkInfo(%opts)
# Comments: Optionally pass in 'user' and 'password' options to force userid and pw
# End-Doc
sub new {
    my $self  = shift;
    my $class = ref($self) || $self;
    my %opts  = @_;

    my $tmp = {};

    $tmp->{db}      = undef;
    $tmp->{subnets} = undef;

    &LogAPIUsage();

    if ( $opts{user} ) {
        $tmp->{user} = $opts{user};
    }
    if ( $opts{password} ) {
        $tmp->{password} = $opts{password};
    }

    return bless $tmp, $class;
}

# Begin-Doc
# Name: SimpleUserRPC
# Type: function
# Description: Helper routine to set up RPC connection for user apps
# Syntax: $rpc = &SimpleUserRPC()
# Access: internal
# End-Doc
sub SimpleUserRPC {
    my $self = shift;

    if ( $self->{user_rpc} ) {
        return $self->{user_rpc};
    }

    my $rpchost;
    my $env = &Local_Env();

    if ( $env eq "dev" ) {
        $rpchost = "itrpc-netdb-dev.mst.edu";
    }
    elsif ( $env eq "test" ) {
        $rpchost = "itrpc-netdb-test.mst.edu";
    }
    else {
        $rpchost = "itrpc-netdb.mst.edu";
    }

    my %rpc_opts = ();
    if ( $self->{user} )     { $rpc_opts{user}     = $self->{user}; }
    if ( $self->{password} ) { $rpc_opts{password} = $self->{password}; }

    $self->{user_rpc} = new Local::SimpleRPC::Client(
        base_url => "http://${rpchost}/perl-bin",
        retries  => 2,
        %rpc_opts
    );

    return $self->{user_rpc};
}

# Begin-Doc
# Name: SimpleAdminRPC
# Type: function
# Description: Helper routine to set up RPC connection for user apps
# Syntax: $rpc = &SimpleAdminRPC()
# Access: internal
# End-Doc
sub SimpleAdminRPC {
    my $self = shift;

    if ( $self->{admin_rpc} ) {
        return $self->{admin_rpc};
    }

    my $rpchost;
    my $env = &Local_Env();

    if ( $env eq "dev" ) {
        $rpchost = "itrpc-netdb-dev.mst.edu";
    }
    elsif ( $env eq "test" ) {
        $rpchost = "itrpc-netdb-test.mst.edu";
    }
    else {
        $rpchost = "itrpc-netdb.mst.edu";
    }

    my %rpc_opts = ();
    if ( $self->{user} )     { $rpc_opts{user}     = $self->{user}; }
    if ( $self->{password} ) { $rpc_opts{password} = $self->{password}; }

    $self->{admin_rpc} = new Local::SimpleRPC::Client(
        base_url => "https://${rpchost}/auth-perl-bin",
        retries  => 2,
        %rpc_opts
    );

    return $self->{admin_rpc};
}

# Begin-Doc
# Name: IPToInteger
# Type: method
# Description: Returns integer form of IP address
# Syntax: $int = $obj->IPToInteger($addr);
# End-Doc
sub IPToInteger {
    my $self = shift;
    my $ip   = shift;

    &LogAPIUsage();

    my @ip = split( /\./, $ip );
    my $res = $ip[0] * 256 * 256 * 256 + $ip[1] * 256 * 256 + $ip[2] * 256 + $ip[3];

    return $res;
}

# Begin-Doc
# Name: IntegerToIP
# Type: method
# Description: Returns dotted quad netmask from a integer IP
# Syntax: $ip = $obj->IntegerToIP($int);
# End-Doc
sub IntegerToIP {
    my $self = shift;
    my $i    = int(shift);

    &LogAPIUsage();

    my ( $a, $b, $c, $d ) = unpack( 'C4', pack( 'N', $i ) );

    return sprintf( "%d.%d.%d.%d", $a, $b, $c, $d );
}

# Begin-Doc
# Name: CondenseEther
# Type: method
# Description: condenses an ethernet address or returns undef
# Syntax: $ether = $obj->CondenseEther($ether)
# End-Doc
sub CondenseEther {
    my $self  = shift;
    my $ether = uc shift;

    $ether =~ tr/0-9A-F//cd;
    if ( length($ether) != 12 ) {
        return undef;
    }
    return $ether;
}

# Begin-Doc
# Name: FormatEther
# Type: method
# Description: formats an ethernet address or returns undef
# Syntax: $ether = $obj->FormatEther($ether)
# End-Doc
sub FormatEther {
    my $self  = shift;
    my $ether = uc shift;

    $ether = $self->CondenseEther($ether);

    $ether =~ s/^(..)(..)(..)(..)(..)(..)/$1:$2:$3:$4:$5:$6/o;
    return $ether;
}

# Begin-Doc
# Name: BitsToMask
# Type: method
# Description: Returns dotted quad netmask from a /bits notation
# Syntax: $mask = $obj->BitsToMask($bits);
# End-Doc
sub BitsToMask {
    my $self = shift;
    my $bits = int(shift);

    &LogAPIUsage();

    if ( $bits < 0 || $bits > 32 ) {
        return undef;
    }

    if ( $bits == 32 ) {
        return "255.255.255.255";
    }
    elsif ( $bits == 0 ) {
        return "0.0.0.0";
    }
    else {
        my $mask = 0xffffffff;
        $mask = $mask << ( 32 - $bits );

        return $self->IntegerToIP($mask);
    }
}

# Begin-Doc
# Name: BitsToWildcard
# Type: method
# Description: Returns dotted quad wildcard from a /bits notation
# Syntax: $wc = $obj->BitsToWildcard($bits);
# End-Doc
sub BitsToWildcard {
    my $self = shift;
    my $bits = int(shift);

    &LogAPIUsage();

    if ( $bits < 0 || $bits > 32 ) {
        return undef;
    }

    if ( $bits == 0 ) {
        return "255.255.255.255";
    }
    elsif ( $bits == 32 ) {
        return "0.0.0.0";
    }
    else {
        my $mask = 0xffffffff;
        $mask = $mask >> $bits;

        return $self->IntegerToIP($mask);
    }
}

# Begin-Doc
# Name: MaskToBits
# Type: method
# Description: Returns number of 1 bits in a netmask
# Syntax: $bits = $obj->MaskToBits($mask);
# End-Doc
sub MaskToBits {
    my $self    = shift;
    my $mask    = shift;
    my $maskint = $self->IPToInteger($mask);
    my $bits    = 0;

    &LogAPIUsage();

    for ( my $i = 0; $i <= 31; $i++ ) {
        if ( $maskint & ( 1 << $i ) ) {
            $bits++;
        }
    }
    return $bits;
}

# Begin-Doc
# Name: HostToOwner
# Type: method
# Description: Returns owner userid for a host
# Syntax: $owner = $obj->HostToOwner($hostname);
# End-Doc
sub HostToOwner {
    my $self = shift;
    my $host = lc shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleUserRPC();
    my $info = $rpc->HostToOwner( host => $host );
    return $info->{$host};
}

# Begin-Doc
# Name: HostToOwnerMulti
# Type: method
# Description: Returns owner userid for a host
# Syntax: $res = $obj->HostToOwnerMulti($hostname, [$hostname2, ...]);
# Returns: hashref keyed on hostname
# End-Doc
sub HostToOwnerMulti {
    my $self = shift;
    my @host = @_;

    &LogAPIUsage();

    my @parms = ();
    foreach my $host (@host) {
        push( @parms, host => lc($host) );
    }

    my $rpc  = $self->SimpleUserRPC();
    my $info = $rpc->HostToOwner(@parms);
    return $info;
}

# Begin-Doc
# Name: OwnerToHosts
# Type: method
# Description: Returns list of hosts owned by a user
# Syntax: @hosts = $obj->OwnerToHosts($userid);
# End-Doc
sub OwnerToHosts {
    my $self   = shift;
    my $userid = lc shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleUserRPC();
    my $info = $rpc->OwnerToHosts( userid => $userid );
    return $info->{$userid};
}

# Begin-Doc
# Name: OwnerToHostsMulti
# Type: method
# Description: Returns hash, keyed on userid, value is array of hosts owned by that user
# Syntax: $hosts = $obj->OwnerToHostsMulti($userid,[$userid,...]);
# End-Doc
sub OwnerToHostsMulti {
    my $self  = shift;
    my @users = @_;

    &LogAPIUsage();

    my @args;
    foreach my $user (@users) {
        push( @args, "userid" => lc $user );
    }

    my $rpc  = $self->SimpleUserRPC();
    my $info = $rpc->OwnerToHosts(@args);
    return $info;
}

# Begin-Doc
# Name: HostToEther
# Type: method
# Description: Returns list of ethernet addresses for a host
# Syntax: @ethers = $obj->HostToEther($hostname);
# End-Doc
sub HostToEther {
    my $self = shift;
    my $host = lc shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleUserRPC();
    my $info = $rpc->HostToEther( host => $host );
    return @{ $info->{$host} };
}

# Begin-Doc
# Name: HostToEtherMulti
# Type: method
# Description: Returns list of ethernet addresses for multiple hosts
# Syntax: %ethers = $obj->HostToEtherMulti($hostname, $hostname, ...);
# End-Doc
sub HostToEtherMulti {
    my $self  = shift;
    my @hosts = @_;

    &LogAPIUsage();

    my $rpc = $self->SimpleUserRPC();

    my @req = ();
    foreach my $host (@hosts) {
        push( @req, "host" => lc($host) );
    }

    my $info = $rpc->HostToEther(@req);
    return $info;
}

# Begin-Doc
# Name: EtherToHost
# Type: method
# Description: Returns hostname with a particular ethernet addr
# Syntax: $host = $obj->EtherToHost($ether);
# End-Doc
sub EtherToHost {
    my $self  = shift;
    my $ether = shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleUserRPC();
    my $info = $rpc->EtherToHost( ether => $ether );
    return $info->{$ether};
}

# Begin-Doc
# Name: LastLeaseToEther
# Type: method
# Description: Returns last ether to lease a particular IP addr
# Syntax: $ether = $obj->LastLeaseToEther($ip);
# End-Doc
sub LastLeaseToEther {
    my $self = shift;
    my $ip   = shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleUserRPC();
    my $info = $rpc->LastLeaseToEther( ip => $ip );
    return $info->{$ip};
}

# Begin-Doc
# Name: LastLeaseToEtherMulti
# Type: method
# Description: Returns last ether to lease a particular IP addr
# Syntax: $res = $obj->LastLeaseToEther($ip1, [$ip2, ...]);
# Returns: hash ref keyed on ip address
# End-Doc
sub LastLeaseToEtherMulti {
    my $self = shift;
    my @ip   = @_;

    &LogAPIUsage();

    my @parms = ();
    foreach my $ip (@ip) {
        push( @parms, ip => $ip );
    }

    my $rpc  = $self->SimpleUserRPC();
    my $info = $rpc->LastLeaseToEther(@parms);
    return $info;
}

# Begin-Doc
# Name: LastLeaseToHost
# Type: method
# Description: Returns last host to lease a particular IP addr
# Syntax: $ether = $obj->LastLeaseToHost($ip);
# End-Doc
sub LastLeaseToHost {
    my $self = shift;
    my $ip   = shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleUserRPC();
    my $info = $rpc->LastLeaseToHost( ip => $ip );
    return $info->{$ip};
}

# Begin-Doc
# Name: LastLeaseToHostMulti
# Type: method
# Description: Returns last host to lease a particular IP addr
# Syntax: $res = $obj->LastLeaseToHost($ip1, [$ip2, ...]);
# Returns: hash ref keyed on ip address
# End-Doc
sub LastLeaseToHostMulti {
    my $self = shift;
    my @ip   = @_;

    &LogAPIUsage();

    my @parms = ();
    foreach my $ip (@ip) {
        push( @parms, ip => $ip );
    }

    my $rpc  = $self->SimpleUserRPC();
    my $info = $rpc->LastLeaseToHost(@parms);
    return $info;
}

# Begin-Doc
# Name: MatchPartialHost
# Type: method
# Description: Returns hostnames containing a substring
# Syntax: @hosts = $obj->MatchPartialHost($pattern);
# End-Doc
sub MatchPartialHost {
    my $self = shift;
    my $pat  = lc shift;

    &LogAPIUsage();

    if ( $pat eq "" || $pat !~ /^[\.a-z0-9-_]+$/o ) {
        return ();
    }

    my $rpc = $self->SimpleUserRPC();
    my $info = $rpc->MatchPartialHost( host => $pat );
    return @$info;
}

# Begin-Doc
# Name: ValidFQDN
# Type: method
# Description: Returns true/nonzero if a host exists
# Syntax: $res = $obj->ValidFQDN($host);
# End-Doc
sub ValidFQDN {
    my $self = shift;
    my $host = lc shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleUserRPC();
    my $info = $rpc->Exists( host => $host );
    return $info->{$host};
}

# Begin-Doc
# Name: GetIPAddress
# Type: method
# Description: Returns first found ip address for a host
# Syntax: $res = $obj->GetIPAddress($host);
# End-Doc
sub GetIPAddress {
    my $self = shift;
    my $host = lc shift;

    &LogAPIUsage();

    my ( $name, $aliases, $addrtype, $length, @addrs ) = gethostbyname($host);

    my ( $a, $b, $c, $d ) = unpack( 'C4', $addrs[0] );
    return sprintf( "%d.%d.%d.%d", $a, $b, $c, $d );
}

# Begin-Doc
# Name: GetHostName
# Type: method
# Description: Returns dns hostname from an ip address
# Syntax: $hn = $obj->GetHostName($ip);
# End-Doc
sub GetHostName {
    my $self = shift;
    my $ip   = lc shift;

    &LogAPIUsage();

    my $iaddr = pack( "C4", split( /\./, $ip ) );
    return gethostbyaddr( $iaddr, 2 );
}

# Begin-Doc
# Name: GetSubnets
# Type: method
# Description: Returns data for all subnets
# Syntax: $subnetinfo = $obj->GetSubnets();
# Comments: caches subnet info details for fast repeated lookup
# End-Doc
sub GetSubnets {
    my $self = shift;

    &LogAPIUsage();

    if ( $self->{subnets} ) {
        return $self->{subnets};
    }

    my $rpc  = $self->SimpleUserRPC();
    my $info = $rpc->GetSubnets();
    return $info;
}

# Begin-Doc
# Name: GetVLANs
# Type: method
# Description: Returns data for all vlans
# Syntax: $vlaninfo = $obj->GetVLANs();
# Comments: caches vlan info details for fast repeated lookup
# End-Doc
sub GetVLANs {
    my $self = shift;

    &LogAPIUsage();

    if ( $self->{vlans} ) {
        return $self->{vlans};
    }

    my $rpc  = $self->SimpleUserRPC();
    my $info = $rpc->GetVLANs();
    return $info;
}

# Begin-Doc
# Name: NetworkSort
# Type: method
# Description: Sorts an array according to network address
# Syntax: @list = $obj->NetworkSort(@list);
# End-Doc
sub NetworkSort {
    my $self  = shift;
    my @items = @_;

    &LogAPIUsage();

    return sort {
        my @aa = split( /[\.\/]/, $a );
        my @bb = split( /[\.\/]/, $b );
        return (   ( $aa[0] <=> $bb[0] )
                || ( $aa[1] <=> $bb[1] )
                || ( $aa[2] <=> $bb[2] )
                || ( $aa[3] <=> $bb[3] )
                || ( $aa[4] <=> $bb[4] ) );
    } @items;
}

# Begin-Doc
# Name: MaskAddress
# Type: method
# Description: Masks an address with a given netmask
# Syntax: $maskedaddr = $obj->MaskAddress($addr, $mask);
# End-Doc
sub MaskAddress {
    my $self = shift;
    my $addr = shift;
    my $mask = shift;

    &LogAPIUsage();

    my $maskint = $self->IPToInteger($mask);
    my $addrint = $self->IPToInteger($addr);

    return $self->IntegerToIP( $addrint & $maskint );
}

# Begin-Doc
# Name: MakeBroadcastAddress
# Type: method
# Description: Calculates the broadcast address for a subnet given mask
# Syntax: $bcaddr = $obj->MakeBroadcastAddress($sn, $mask);
# End-Doc
sub MakeBroadcastAddress {
    my $self   = shift;
    my $subnet = shift;
    my $mask   = shift;

    &LogAPIUsage();

    my $maskint   = $self->IPToInteger($mask);
    my $subnetint = $self->IPToInteger($subnet);

    my $bcint = $subnetint | ~$maskint;
    my $bc    = $self->IntegerToIP($bcint);

    return $bc;
}

# Begin-Doc
# Name: MakeGatewayAddress
# Type: method
# Description: Calculates the standard gateway (bc-1) address for a subnet given mask
# Syntax: $gwaddr = $obj->MakeGatewayAddress($sn, $mask);
# End-Doc
sub MakeGatewayAddress {
    my $self   = shift;
    my $subnet = shift;
    my $mask   = shift;

    &LogAPIUsage();

    my $maskint   = $self->IPToInteger($mask);
    my $subnetint = $self->IPToInteger($subnet);

    my $bcint = $subnetint | ~$maskint;
    my $gwint = $bcint & ~0x0000001;

    my $gw = $self->IntegerToIP($gwint);

    return $gw;
}

# Begin-Doc
# Name: GetAddressNetworkInfo
# Type: method
# Description: Returns netmask, broadcast, network, and gateway for a given ip
# Syntax: %info = $obj->GetAddressNetworkInfo($addr);
# End-Doc
sub GetAddressNetworkInfo {
    my $self = shift;
    my $addr = shift;

    my $addrint     = $self->IPToInteger($addr);
    my $addrfromint = $self->IntegerToIP($addrint);

    if ( $addr ne $addrfromint ) {
        return ();
    }

    my $subnets = $self->GetSubnets();
    foreach my $sname ( keys(%$subnets) ) {
        my $snaddr = $sname;
        $snaddr =~ s|/.*||gio;

        my $mask = $subnets->{$sname}->{mask};

        if ( $self->MaskAddress( $addr, $mask ) eq $snaddr ) {
            return (
                subnet    => $sname,
                address   => $addr,
                netmask   => $mask,
                network   => $snaddr,
                gateway   => $self->MakeGatewayAddress( $snaddr, $mask ),
                broadcast => $self->MakeBroadcastAddress( $snaddr, $mask ),
            );
        }
    }

    return ();
}

# Begin-Doc
# Name: GetHostLocation
# Type: method
# Description: Returns location info field for host
# Syntax: $status = $obj->GetHostLocation($hostname);
# End-Doc
sub GetHostLocation {
    my $self = shift;
    my $host = lc shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    my $info = $rpc->GetHostLocation( host => $host );
    return $info->{$host}->{location};
}

# Begin-Doc
# Name: GetHostDescription
# Type: method
# Description: Returns description info field for host
# Syntax: $status = $obj->GetHostDescription($hostname);
# End-Doc
sub GetHostDescription {
    my $self = shift;
    my $host = lc shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    my $info = $rpc->GetHostDescription( host => $host );
    return $info->{$host}->{description};
}

# Begin-Doc
# Name: GetUtilityCNames
# Type: method
# Description: Gets hash to hashes of utility cnames in particular groups
# Syntax: $ref = $obj->GetUtilityCNames($grp1, [$grp2,...])
# End-Doc
sub GetUtilityCNames {
    my $self   = shift;
    my @groups = @_;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    my @req;
    foreach my $grp (@groups) {
        push( @req, "group" => $grp );
    }
    my @res = $rpc->GetUtilityCNames(@req);
    return {@res};
}

# Begin-Doc
# Name: DeleteUtilityCNames
# Type: method
# Description: Deletes a list of utility cname hosts
# Syntax: $res = $obj->DeleteUtilityCNames($host1, [$host2,...])
# Comments: No return unless failure, which will generate a die
# End-Doc
sub DeleteUtilityCNames {
    my $self  = shift;
    my @hosts = @_;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    my @req;
    foreach my $host (@hosts) {
        push( @req, "host" => $host );
    }
    my @res = $rpc->DeleteUtilityCNames(@req);
    return {@res};
}

# Begin-Doc
# Name: UpdateUtilityCName
# Type: method
# Description: Updates or creates a utility cname host
# Syntax: $res = $obj->UpdateUtilityCName($host, $tgt);
# Comments: No return unless failure, which will generate a die
# End-Doc
sub UpdateUtilityCName {
    my $self = shift;
    my $host = shift;
    my $tgt  = shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    return $rpc->UpdateUtilityCName( host => $host, target => $tgt );
}

# Begin-Doc
# Name: GetHostsWithAdminOption
# Type: method
# Description: Gets list of hosts with a given admin option
# Syntax: $res = $obj->GetHostsWithAdminOption($option1, [$option2, ...]);
# Comments: Ref to hash, keys are options, values are array refs with list of hosts
# End-Doc
sub GetHostsWithAdminOption {
    my $self    = shift;
    my @options = @_;

    &LogAPIUsage();

    my $rpc  = $self->SimpleAdminRPC();
    my @args = ();
    foreach my $option (@options) {
        push( @args, "option" => $option );
    }
    return $rpc->GetHostsWithAdminOption(@args);
}

# Begin-Doc
# Name: GetHostsWithDHCPOption
# Type: method
# Description: Gets list of hosts with a given dhcp option
# Syntax: $res = $obj->GetHostsWithDHCPOption($option1, [$option2, ...]);
# Comments: Ref to hash, keys are options, values are array refs with list of hosts
# End-Doc
sub GetHostsWithDHCPOption {
    my $self    = shift;
    my @options = @_;

    &LogAPIUsage();

    my $rpc  = $self->SimpleAdminRPC();
    my @args = ();
    foreach my $option (@options) {
        push( @args, "option" => $option );
    }
    return $rpc->GetHostsWithDHCPOption(@args);
}

# Begin-Doc
# Name: GetAdminOptions
# Type: method
# Description: Gets admin options for a list of hosts
# Syntax: $res = $obj->GetAdminOptions($host1, [$host2, ...]);
# Comments: Ref to hash, keys are hostnames, values are array refs with list of options
# End-Doc
sub GetAdminOptions {
    my $self  = shift;
    my @hosts = @_;

    &LogAPIUsage();

    my $rpc  = $self->SimpleAdminRPC();
    my @args = ();
    foreach my $host (@hosts) {
        push( @args, "host" => $host );
    }
    return $rpc->GetAdminOptions(@args);
}

# Begin-Doc
# Name: GetHostOptions
# Type: method
# Description: Gets dhcp options for a list of hosts
# Syntax: $res = $obj->GetHostOptions($host1, [$host2, ...]);
# Comments: Ref to hash, keys are hostnames, values are array refs with list of options
# End-Doc
sub GetHostOptions {
    my $self  = shift;
    my @hosts = @_;

    &LogAPIUsage();

    my $rpc  = $self->SimpleAdminRPC();
    my @args = ();
    foreach my $host (@hosts) {
        push( @args, "host" => $host );
    }
    return $rpc->GetHostOptions(@args);
}

# Begin-Doc
# Name: SetHostDescription
# Type: method
# Description: Updates host description
# Syntax: $res = $obj->SetHostDescription($host, $desc);
# Comments: No return unless failure, which will generate a die
# End-Doc
sub SetHostDescription {
    my $self = shift;
    my $host = shift;
    my $desc = shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    return $rpc->SetHostDescription( host => $host, description => $desc );
}

# Begin-Doc
# Name: SetHostLocation
# Type: method
# Description: Updates host location
# Syntax: $res = $obj->SetHostLocation($host, $location);
# Comments: No return unless failure, which will generate a die
# End-Doc
sub SetHostLocation {
    my $self = shift;
    my $host = shift;
    my $loc  = shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    return $rpc->SetHostLocation( host => $host, location => $loc );
}

# Begin-Doc
# Name: AddEther
# Type: method
# Description: Adds ethernet address to host
# Syntax: $res = $obj->AddEther($host, $ether);
# Comments: No return unless failure, which will generate a die
# End-Doc
sub AddEther {
    my $self  = shift;
    my $host  = shift;
    my $ether = shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    return $rpc->AddEther( host => $host, ether => $ether );
}

# Begin-Doc
# Name: AddHostOption
# Type: method
# Description: Adds dhcp option to host
# Syntax: $res = $obj->AddHostOption($host, $option);
# Comments: No return unless failure, which will generate a die
# End-Doc
sub AddHostOption {
    my $self   = shift;
    my $host   = shift;
    my $option = shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    return $rpc->AddHostOption( host => $host, option => $option );
}

# Begin-Doc
# Name: RemoveHostOption
# Type: method
# Description: Remove dhcp option from host
# Syntax: $res = $obj->RemoveHostOption($host, $option);
# Comments: No return unless failure, which will generate a die
# End-Doc
sub RemoveHostOption {
    my $self   = shift;
    my $host   = shift;
    my $option = shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    return $rpc->RemoveHostOption( host => $host, option => $option );
}

# Begin-Doc
# Name: AddAdminOption
# Type: method
# Description: Adds admin option to host
# Syntax: $res = $obj->AddAdminOption($host, $option);
# Comments: No return unless failure, which will generate a die
# End-Doc
sub AddAdminOption {
    my $self   = shift;
    my $host   = shift;
    my $option = shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    return $rpc->AddAdminOption( host => $host, option => $option );
}

# Begin-Doc
# Name: RemoveAdminOption
# Type: method
# Description: Remove admin option from host
# Syntax: $res = $obj->RemoveAdminOption($host, $option);
# Comments: No return unless failure, which will generate a die
# End-Doc
sub RemoveAdminOption {
    my $self   = shift;
    my $host   = shift;
    my $option = shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    return $rpc->RemoveAdminOption( host => $host, option => $option );
}

# Begin-Doc
# Name: RemoveEther
# Type: method
# Description: Removes ethernet address to host
# Syntax: $res = $obj->RemoveEther($host, $ether);
# Comments: No return unless failure, which will generate a die
# End-Doc
sub RemoveEther {
    my $self  = shift;
    my $host  = shift;
    my $ether = shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    return $rpc->RemoveEther( host => $host, ether => $ether );
}

# Begin-Doc
# Name: AutoAllocateVMWareAddr
# Type: method
# Description: Automatically allocates a vmware mac address for a host
# Syntax: $addr = $obj->AutoAllocateVMWareAddr($host)
# Comments: Will return new mac on success, or die on error
# End-Doc
sub AutoAllocateVMWareAddr {
    my $self = shift;
    my $host = shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    return $rpc->AutoAllocateVMWareAddr( host => $host );
}

# Begin-Doc
# Name: DeleteHost
# Type: method
# Description: Deletes a host
# Syntax: $addr = $obj->DeleteHost($host)
# Comments: No return on success dies on error
# End-Doc
sub DeleteHost {
    my $self = shift;
    my $host = shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    return $rpc->DeleteHost( host => $host );
}

# Begin-Doc
# Name: CreateHost
# Type: method
# Description: Creates a host
# Syntax: $addr = $obj->CreateHost(%opts)
# Comments: On success, returned a few attributes - host, domain, owner, type), dies on error,
# Comments: %opts has various fields, refer to the netdb NetDBAdmin.pm RPC module for
# full details. Attributes (owner, index, hostname, hosttype, nametype, host, domain)
# End-Doc
sub CreateHost {
    my $self = shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    return $rpc->CreateHost(@_);
}

# Begin-Doc
# Name: GetAdminComment
# Type: method
# Description: Returns admin comment for a host
# Syntax: $comment = $obj->GetAdminComment($host);
# End-Doc
sub GetAdminComment {
    my $self = shift;
    my $host = lc shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    my $info = $rpc->GetAdminComment( host => $host );
    return $info->{$host};
}

# Begin-Doc
# Name: SetAdminComment
# Type: method
# Description: Sets admin comment for a host, clears if undef
# Syntax: $obj->SetAdminComment($host => $comment)
# End-Doc
sub SetAdminComment {
    my $self = shift;
    my %opts = @_;

    &LogAPIUsage();

    my $rpc  = $self->SimpleAdminRPC();
    my $info = $rpc->SetAdminComment(%opts);
    return $info;
}

# Begin-Doc
# Name: GetHostMetadataField
# Type: method
# Description: Returns field content for a host
# Syntax: $value = $obj->GetHostMetadataField($host, $field);
# End-Doc
sub GetHostMetadataField {
    my $self  = shift;
    my $host  = lc shift;
    my $field = shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    my $info = $rpc->GetHostMetadataField( host => $host, field => $field );
    return $info->{$host}->{$field};
}

# Begin-Doc
# Name: GetHostMetadataFieldAll
# Type: method
# Description: Returns field content for all host
# Syntax: %hosts = $obj->GetHostMetadataFieldAll($field);
# End-Doc
sub GetHostMetadataFieldAll {
    my $self  = shift;
    my $host  = lc shift;
    my $field = shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    my $info = $rpc->GetHostMetadataFieldAll( field => $field );
    return $info;
}

# Begin-Doc
# Name: SetHostMetadataField
# Type: method
# Description: Sets field content for a host
# Syntax: $obj->SetHostMetadataField($host, $field, $value);
# End-Doc
sub SetHostMetadataField {
    my $self  = shift;
    my $host  = lc shift;
    my $field = shift;
    my $value = shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    my $info = $rpc->SetHostMetadataField( host => $host, field => $field, value => $value );
    return undef;
}

# Begin-Doc
# Name: GetAdminLock
# Type: method
# Description: Returns non-zero for host if it is admin locked
# Syntax: $lock = $obj->GetAdminLock($host)
# End-Doc
sub GetAdminLock {
    my $self = shift;
    my $host = lc shift;

    &LogAPIUsage();

    my $rpc = $self->SimpleAdminRPC();
    my $info = $rpc->GetAdminLock( host => $host );
    return $info->{$host};
}

# Begin-Doc
# Name: SetAdminLock
# Type: method
# Description: Sets admin lock status for a host, non-zero is locked
# Syntax: $obj->SetAdminLock($host => $lockstatus)
# End-Doc
sub SetAdminLock {
    my $self = shift;
    my %opts = @_;

    &LogAPIUsage();

    my $rpc  = $self->SimpleAdminRPC();
    my $info = $rpc->SetAdminLock(%opts);
    return $info;
}

1;
