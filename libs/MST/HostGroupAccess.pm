
=begin
Begin-Doc
Name: MST::HostGroupAccess
Type: module
Description: NIS sub-administration access control
Comments: These routines allow an application to determine what userids are allow
to administer (add/remove users to/from) hostgroups.

The access is determined using the privsys codes that start with
"hostgroup-mgmt:".

These routines use REMOTE_USER if $user is not defined or not given.
End-Doc
=cut

package MST::HostGroupAccess;
require Exporter;
use strict;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

@ISA    = qw(Exporter);
@EXPORT = qw(
    HostGroupAccess_PrivCode
    HostGroupAccess_Check
    HostGroupAccess_List
);

use Local::PrivSys;
use MST::HostGroup;
use Local::UsageLogger;

# Begin-Doc
# Name: HostGroupAccess_PrivCode
# Type: function
# Description: Returns priv code associated with admin access for a hostgroup
# Syntax: $code = &HostGroupAccess_PrivCode($group)
# End-Doc
sub HostGroupAccess_PrivCode {
    my ($group) = @_;

    &LogAPIUsage();

    my $tmp = $group;
    $tmp =~ s/-+/:/g;

    return "hostgroup-mgmt:$tmp";
}

# Begin-Doc
# Name: HostGroupAccess_Check
# Type: function
# Description: Returns non-zero if the user is allowed to administer the group
# Syntax: $res = &HostGroupAccess_Check($group[,$user])
# Comments: if user is not specified, uses current REMOTE_USER
# End-Doc
sub HostGroupAccess_Check {
    my $group = shift;
    my $user = shift || $ENV{REMOTE_USER};

    &LogAPIUsage();

    my %privs = &PrivSys_FetchPrivs($user);

    if ( $privs{"hostgroup-mgmt"} ) {
        return 1;
    }

    my @tmp = split( /-+/, $group );
    for ( my $i = 0; $i <= $#tmp; $i++ ) {
        my $tmpcode = "hostgroup-mgmt:" . join( ":", @tmp[ 0 .. $i ] );
        if ( $privs{$tmpcode} ) {
            return 1;
        }
    }
    return 0;
}

# Begin-Doc
# Name: HostGroupAccess_List
# Type: function
# Description: Returns a list of groups the user is allowed to administer
# Syntax: @res = &HostGroupAccess_List([$user]);
# Comments: if user is not specified, uses current REMOTE_USER
# End-Doc
sub HostGroupAccess_List {
    my $user = shift || $ENV{REMOTE_USER};

    &LogAPIUsage();

    my %privs = &PrivSys_FetchPrivs($user);

    my @access;
GROUP: foreach my $group (&NetGroup_List) {

        if ( $privs{"hostgroup-mgmt"} ) {
            push( @access, $group );
            next GROUP;
        }

        my @tmp = split( /-+/, $group );
        for ( my $i = 0; $i <= $#tmp; $i++ ) {
            my $tmpcode = "hostgroup-mgmt:" . join( ":", @tmp[ 0 .. $i ] );
            if ( $privs{$tmpcode} ) {
                push( @access, $group );
                next GROUP;
            }
        }
    }

    return @access;
}

1;
