
=begin

Begin-Doc
Name: MST::UM::AIMS
Type: module
Description: utility wrapper for signing calls to AIMS web ui
Requires: access to AIMS, stashed 'aims-apikey' and 'aims-appid' in authsrv
End-Doc

=cut

package MST::UM::AIMS;
require Exporter;
use strict;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

@ISA    = qw(Exporter);
@EXPORT = qw(
);

use Local::AuthSrv;
use Local::UsageLogger;
use Local::CurrentUser;
use Carp;
use UUID;
use Digest::SHA qw(hmac_sha256);
use Digest::MD5 qw(md5);
use MIME::Base64 qw(decode_base64 encode_base64);
use URI::Escape;
use LWP;
use JSON;

BEGIN {
    &LogAPIUsage();
}

# Begin-Doc
# Name: new
# Type: function
# Description: Creates object for calls to UM AIMS
# Syntax: $ex = new MST::UM::AIMS([debug => '0|1'], env => "qa|production") || die;
# End-Doc
sub new {
    my $self  = shift;
    my $class = ref($self) || $self;
    my %info  = @_;
    my $debug = $info{debug} || 0;
    my $env   = $info{env} || $info{environment} || "must specify";

    &LogAPIUsage();

    # set any object params
    my $tmp = {};
    $tmp->{"debug"} = $debug;

    my $baseurl;
    my $suffix;
    if ( $env eq "qa" ) {
        $baseurl = "https://aims-qa-iis.qa-um.umsystem.edu/AuthoritativeWebService";
        $suffix = "-qa";
    }
    elsif ( $env eq "production" ) {
        $baseurl = "https://appsprod.missouri.edu/authoritativewebservice";
    }
    else {
        croak "unhandled environment ($env)";
    }

    $tmp->{baseurl} = $baseurl;
    $tmp->{api_key_binary} = decode_base64( &AuthSrv_Fetch( instance => "aims-apikey${suffix}" ) );
    if ( !$tmp->{api_key_binary} ) { croak "Must have aims-apikey${suffix} stash." }

    $tmp->{appid} = &AuthSrv_Fetch( instance => "aims-appid${suffix}" );
    if ( !$tmp->{appid} ) { croak "Must have aims-appid${suffix} stash." }

    my $ua = new LWP::UserAgent;
    $ua->agent("MST-AIMSClient/1.0");
    $tmp->{ua} = $ua;


    return bless $tmp, $class;
}

# Begin-Doc
# Name: debug
# Type: method
# Syntax: $res = $aims->debug($value)
# Comments: Enables debugging, if no value, returns current state
# End-Doc
sub debug {
    my $self = shift;
    if (@_) {
        $self->{debug} = shift;
    }
    else {
        return $self->{debug};
    }
}

# Begin-Doc
# Name: get_sso_key
# Type: method
# Syntax: $key = $aims->get_sso_key($ssoid)
# Returns: helper function to retrieve sso key for a userid, undef if failure or not found
# End-Doc
sub get_sso_key {
    my $self  = shift;
    my $ssoid = shift;

    my ( $err, $res ) = $self->request(
        method => "GET",
        action => "api/v1/account/SSOAccountInformation",
        query  => "ssoid=$ssoid",
    );
    if ( !$err && ref($res) eq "HASH" ) {
        return $res->{SSOKey};
    }
    else {
        return undef;
    }
}

# Begin-Doc
# Name: get_reserved_uid
# Type: method
# Syntax: $key = $aims->get_reserved_uid($ssoid)
# Returns: helper function to retrieve reserved uid for a userid/group/id, undef if failure or not found
# End-Doc
sub get_reserved_uid {
    my $self  = shift;
    my $ssoid = shift;

    my ( $err, $res ) = $self->request(
        method => "GET",
        action => "api/v1/account/SSOAccountInformation",
        query  => "ssoid=$ssoid",
    );
    if ( !$err && ref($res) eq "HASH" ) {
        return $res->{ReservedUID};
    }
    else {
        return undef;
    }
}

# Begin-Doc
# Name: request
# Type: method
# Syntax: ($err,$res) = $aims->request(method => "GET|POST", action => "api/account/createadmin", content => {...}, query => "query_parms_after_questionmark");
# Returns: err is non-empty on error, res = decoded json or undef
# End-Doc
sub request {
    my $self = shift;
    my %opts = @_;

    my $method  = $opts{method};
    my $action  = $opts{action};
    my $content = $opts{content};
    my $query   = $opts{query};

    my $baseurl        = $self->{baseurl};
    my $ua             = $self->{ua};
    my $api_key_binary = $self->{api_key_binary};
    my $appid          = $self->{appid};

    my $url = "$baseurl/$action";
    if ($query) {
        $url .= "?" . $query;
    }

    # Sane default
    if ( $content && !$method ) {
        $method = "POST";
    }
    elsif ( !$method ) {
        $method = "GET";
    }

    my $nonce;
    eval { $nonce = UUID::uuid(); };
    if ( !$nonce ) {
        my $uuid;
        UUID::generate($uuid);
        UUID::unparse( $uuid, $nonce );
    }
    if ( !$nonce ) {
        croak "Couldn't generate nonce.\n";
    }

    # force to integer in case of Time::HiRes
    my $ts = int(time);
    my $req_content;
    if ($content) {
        $req_content = encode_json($content);
    }

    my $req_content_hash_base64 = "";
    if ($req_content) {
        $req_content_hash_base64 = encode_base64( md5($req_content) );
        chomp($req_content_hash_base64);
    }

    my $sigdata = join( "", $appid, $method, lc( uri_escape( lc($url) ) ), $ts, $nonce, $req_content_hash_base64 );
    my $sig = hmac_sha256( $sigdata, $api_key_binary );
    my $sig_base64 = encode_base64($sig);
    chomp($sig_base64);

    my $req = HTTP::Request->new( $method => $url );
    $req->header( "Authorization" => "amx ${appid}:${sig_base64}:${nonce}:${ts}" );
    if ($req_content) {
        $req->content_type("application/json");
        $req->content($req_content);
    }

    if ( $self->debug ) {
        print "\n\nRequest:\n";
        print $req->as_string, "\n\n";
    }

    my $res = $ua->request($req);
    if ( $self->debug ) {
        print "\n\nResponse:\n";
        print $res->as_string, "\n\n";
    }

    if ( !$res->is_success ) {
        return ( $res->as_string, {} );
    }

    my $content = $res->content;
    my $decoded;
    eval { $decoded = decode_json($content); };
    if ( !$decoded ) {
        return ( $@, {} );
    }
    else {
        return ( undef, $decoded );
    }
}

1;
