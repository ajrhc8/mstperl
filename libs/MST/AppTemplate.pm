
=begin

Begin-Doc
Name: MST::AppTemplate
Type: module
Description: child class around Local::AppTemplate to set defaults

End-Doc

=cut

package MST::AppTemplate;
require 5.000;
use Exporter;
use strict;
use Local::AppTemplate;
use MST::Env;
use MST::UsageLogger;

use vars qw (@ISA @EXPORT);
@ISA    = qw(Local::AppTemplate Exporter);
@EXPORT = qw();

=begin
Begin-Doc
Name: new
Type: method
Description: creates object
Syntax: $obj->new(%params)
Comments: Same syntax/behavior as routine in AppTemplate module.
End-Doc
=cut

sub new {
    my $self = shift;
    my @args = @_;
    my %opts = @_;

    my %base_opts = (
        title         => "MST Software",
        contact_url   => "https://help.mst.edu",
        contact_label => "IT HelpDesk",
        app_env       => &MST_Env(),
    );

    if ( !defined( $ENV{APPTEMPLATE_PATH} ) ) {
        $base_opts{template_path} = ["https://apptemplate.mst.edu/v3"];
    }

    return $self->SUPER::new( %base_opts, @_ );
}

1;