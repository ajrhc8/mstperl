
=begin

Begin-Doc
Name: MST::AppMenu
Type: module
Description: child class around Local::AppMenu to override behaviors as needed

End-Doc

=cut

package MST::AppMenu;
require 5.000;
use Exporter;
use strict;
use Local::AppMenu;
use MST::Env;
use MST::UsageLogger;

use vars qw (@ISA @EXPORT);
@ISA    = qw(Local::AppMenu Exporter);
@EXPORT = qw();

=begin
Begin-Doc
Name: new
Type: method
Description: creates object
Syntax: $obj->new(%params)
Comments: Same syntax/behavior as routine in AppMenu module.
End-Doc
=cut

sub new {
    my $self = shift;

    return $self->SUPER::new(@_);
}

1;