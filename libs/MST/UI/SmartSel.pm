# Begin-Doc
##################################################################
# Name: SmartSel.pm
# Type: Library
# Description: provide smart selection on the html list
#              As you type the char in the list box, it automatically moves the first match.
# Functions:  isIE                   - check if the brower is IE or not
#             SmartSelStartForm      - start the html form for the smart selection
#             SmartSelStart          - provide java script code that shows the smart select result
#             SmartSelSelectItem     - provide html tag for the list box
#             SmartSelEnd            - close the smart select tag
#             SmartSelEndForm        - close the form
##################################################################
# End-Doc

package MST::UI::SmartSel;
require Exporter;
use strict;

use vars qw (@ISA @EXPORT);

@ISA    = qw(Exporter);
@EXPORT = qw(
    SmartSelStartForm
    SmartSelStart
    SmartSelSelectItem
    SmartSelEnd
    SmartSelEndForm
);

use MST::UsageLogger;

BEGIN {
    &LogAPIUsage();
}

# Begin-Doc
##################################################################
# Name: SmartSelStartForm
# Type: Function
# Syntax: &SmartSelStartForm($action_url)
# Description: start the html form for the smart selection
# Passed: $action_url
# Returned: none
##################################################################
# End-Doc
sub SmartSelStartForm {
    my %opt = @_;

    &LogAPIUsage();

    print q {
        <script type="text/javascript">
        <!--

        var str = new String;
        var CurrOption = 0;
        var IE = /*@cc_on!@*/false;

        function resetStr(ctrl) {
            CurrOption = 0;
            str = "";
            status = "";

            if(!IE) {
                ctrl.value = "";
            }
        }

        function getOneKey(evt) {
            if ( IE ) {
                return window.event.keyCode;
            }
            else {
                return evt.which;
            }
        }

        function doSearch(searchstr,form,selectbox,reverse) {
            searchstr.replace('^\\s*','');
            if (searchstr) {
              status = "Search String: " + searchstr;
            }
            else {
              status = "";
            }
            pattern1 = new RegExp("^"+searchstr,"i");

            if (reverse) {
                for (var i=0;i<CurrOption;i++) {
                    if (pattern1.test(form.elements[selectbox].options[i].text)) {
                        form.elements[selectbox].options[i].selected = true;
                        EndingPoint = CurrOption + 1;
                        CurrOption = i;
                        i = EndingPoint;
                    }
                }
            }
            else {
                num_options = form.elements[selectbox].options.length;
                for (var i=CurrOption;i<num_options;i++) {
                    if (pattern1.test(form.elements[selectbox].options[i].text)) {
                        form.elements[selectbox].options[i].selected = true;
                        CurrOption = i;
                        i = num_options+1;
                    }
                }
            }
        }

        if ( IE ) {
            function noBkSpc(evt,form,selectbox) {
                oneKey = getOneKey(evt);
                if (oneKey == 8) {
                    str = str.substr(0, str.length-1);
                    doSearch(str,form,selectbox,1);
                    window.event.returnValue = false;
                }
            }
        }

        function selSearch(evt,form,selectbox) {
            var reverse = 0;
            var oneKey = getOneKey(evt);

            if ( IE ) {
                str += String.fromCharCode(oneKey);
            }
            else {
                if (oneKey == 8) {
                    str = str.substr(0, str.length-1);
                    reverse = 1;
                }
                else {
                    str += String.fromCharCode(oneKey);
                    reverse = 0;
                }
            };

            doSearch(str,form,selectbox,reverse);

            if ( IE ) {
                window.event.returnValue = false;
            }
        }

        if ( !IE ) {
            document.captureEvents(Event.KEYPRESS);
            document.onkeypress = function (evt) {
                                document.routeEvent(evt);
                                return true;
                              };
        }
        // -->
        </script>
        <form method=post
	};

    foreach my $key ( keys %opt ) {
        print qq { $key = "$opt{$key}" };
    }
    print qq { >};
}

# Begin-Doc
##################################################################
# Name: SmartSelStart
# Type: Function
# Syntax: &SmartSelStart($control_desc,$select_name,$text_length,$select_height,$dont_disp_desc)
# Description: provide java script code that shows the smart select result
# Passed: $control_desc - Text description that is displayed beside the control
#         $select_name - String that is printed as the name of the select control
#         $text_length - Length of the search textbox required for Netscape
#         $select_height - Height of the select control (1=dropdown list, >1=select list)
#         $dont_disp_desc - If =1, don't display the text description
# Returned: none
##################################################################
# End-Doc
sub SmartSelStart {
    my ($control_desc,  #Text description that is displayed beside the control
        $select_name
        ,    #String that is printed as the name of the select control
        $text_length,    #Length of the search textbox required for Netscape
        $select_height
        ,    #Height of the select control (1=dropdown list, >1=select list)
        $dont_disp_desc    #If =1, don't display the text description
    ) = @_;

    my $script;
    my $blurscript;

    &LogAPIUsage();

    if ( $ENV{'HTTP_USER_AGENT'} =~ /MSIE/ ) {
        $script
            = qq {javascript:selSearch(event, this.form, '$select_name');};
        my $downscript
            = qq {javascript:noBkSpc(event, this.form, '$select_name');};
        $blurscript = qq{javascript:resetStr();};
        unless ($dont_disp_desc) {
            print qq{$control_desc : };
        }
        print qq {<select name="$select_name" size="$select_height"
                  onKeyPress = "$script", onKeyDown = "$downscript",
                  onBlur = "$blurscript">};
    }
    else {
        my $text_name = $select_name . "_search";
        $script
            = qq {javascript:selSearch(event, this.form, '$select_name');};
        $blurscript = qq {javascript:resetStr(this);};
        unless ($dont_disp_desc) {
            print qq {$control_desc Search : };
        }
        print
            qq {<input type="text" name="$text_name" size="$text_length" onKeyPress="$script" onBlur = "$blurscript"><br />};
        unless ($dont_disp_desc) {
            print "$control_desc : ";
        }
        if ( $select_height ne "" ) {
            print qq {<select name="$select_name" size="$select_height">};
        }
        else {
            print qq {<select name="$select_name">};
        }
    }
}

# Begin-Doc
##################################################################
# Name: SmartSelSelectItem
# Type: Function
# Syntax: &SmartSelSelectItem($value, $name, $selected)
# Description: provide html tag for the list box
# Passed: $value, $name, $selected
# Returned: none
##################################################################
# End-Doc
sub SmartSelSelectItem {
    my ( $value, $name, $selected ) = @_;

    print qq{<option value="$value"};

    if ($selected) {
        print qq{ selected };
    }

    print qq{>$name};
}

# Begin-Doc
##################################################################
# Name: SmartSelEnd
# Type: Function
# Syntax: &SmartSelEnd()
# Description: close the smart select tag
# Passed: none
# Returned: none
##################################################################
# End-Doc
sub SmartSelEnd {
    print "</select>\n";
}

# Begin-Doc
##################################################################
# Name: SmartSelEndForm
# Type: Function
# Syntax: &SmartSelEndForm()
# Description: close the smart select form
# Passed: none
# Returned: none
##################################################################
# End-Doc
sub SmartSelEndForm {
    print "</form>\n";
}
