
=begin
Begin-Doc
Name: MST::NetGroupAccess
Type: module
Description: NIS sub-administration access control
Comments: These routines allow an application to determine what userids are allow
to administer (add/remove users to/from) netgroups.

The access is determined using the privsys codes that start with
"netgroup-mgmt:".

These routines use REMOTE_USER if $user is not defined or not given.
End-Doc
=cut

package MST::NetGroupAccess;
require Exporter;
use strict;

use vars qw($VERSION @ISA @EXPORT @EXPORT_OK);

@ISA    = qw(Exporter);
@EXPORT = qw(
    NetGroupAccess_PrivCode
    NetGroupAccess_Check
    NetGroupAccess_List
);

use Local::PrivSys;
use MST::NetGroup;
use Local::UsageLogger;

# Begin-Doc
# Name: NetGroupAccess_PrivCode
# Type: function
# Description: Returns priv code associated with admin access for a netgroup
# Syntax: $code = &NetGroupAccess_PrivCode($group)
# End-Doc
sub NetGroupAccess_PrivCode {
    my ($group) = @_;

    &LogAPIUsage();

    my $tmp = $group;
    $tmp =~ s/-+/:/g;

    return "netgroup-mgmt:$tmp";
}

# Begin-Doc
# Name: NetGroupAccess_Check
# Type: function
# Description: Returns non-zero if the user is allowed to administer the group
# Syntax: $res = &NetGroupAccess_Check($group[,$user])
# Comments: if user is not specified, uses current REMOTE_USER
# End-Doc
sub NetGroupAccess_Check {
    my $group = shift;
    my $user = shift || $ENV{REMOTE_USER};

    &LogAPIUsage();

    my %privs = &PrivSys_FetchPrivs($user);

    if ( $privs{"netgroup-mgmt"} )
    {
        return 1;
    }

    my @tmp = split( /-+/, $group );
    for ( my $i = 0; $i <= $#tmp; $i++ ) {
        my $tmpcode = "netgroup-mgmt:" . join( ":", @tmp[ 0 .. $i ] );
        if ( $privs{$tmpcode} ) {
            return 1;
        }
    }
    return 0;
}

# Begin-Doc
# Name: NetGroupAccess_List
# Type: function
# Description: Returns a list of groups the user is allowed to administer
# Syntax: @res = &NetGroupAccess_List([$user]);
# Comments: if user is not specified, uses current REMOTE_USER
# End-Doc
sub NetGroupAccess_List {
    my $user = shift || $ENV{REMOTE_USER};

    &LogAPIUsage();

    my %privs = &PrivSys_FetchPrivs($user);

    my @access;
GROUP: foreach my $group (&NetGroup_List) {

        if ( $privs{"netgroup-mgmt"} )
        {
            push(@access, $group);
            next GROUP;
        }

        my @tmp = split( /-+/, $group );
        for ( my $i = 0; $i <= $#tmp; $i++ ) {
            my $tmpcode = "netgroup-mgmt:" . join( ":", @tmp[ 0 .. $i ] );
            if ( $privs{$tmpcode} ) {
                push( @access, $group );
                next GROUP;
            }
        }
    }

    return @access;
}

1;
